# Mapping server
Mapping the difference between engineering and biology since 2019. Refactored in 2022

## Running it in docker
In order to have a similar build available on the server and on the local development machine, we build the docker image without including the monty source code. This is loaded by the entrypoint when the container is starting. This allows more rapid iteration, but currently we still have to find out if gunicorn allows restarting the code when it changes.

You first should clone the code for fish and into the server directory:

````commandline
git clone https://gitlab.com/Maox/fish_server
cd fish_server
git clone https://gitlab.com/Maox/fish
````

Use docker to build the container to easily distribute this to your servers.

# On windows:
````commandline

docker compose -f docker-compose.local.yml up


docker build -t mapping_server:latest .

run -v "C:\Users\u0113054\Box Sync\Source\monty":/monty -v "C:\Users\u0113054\Box Sync\Source\mapping_server":/app -v "C:\Workdir\unprotected_data\aves_data":/aves_data --network host mapping_server:latest

docker run -v "C:\Users\u0113054\Box Sync\Source\monty":/monty -v "C:\Users\u0113054\Box Sync\Source\mapping_server":/app -it mapping_server:latest /bin/bash
````

´´´
docker pull  

registry.gitlab.mech.kuleuven.be/u0113054/mapping/mapping_base:1.0
 
java -Xmx2G -jar ./app.jar --properties ./properties.conf --server
´´´

## Development
As we need the packages to be available for the docker environment, we make a symlink between them and this environment.
````commandline
mklink /J "C:\Users\u0113054\OneDrive - KU Leuven\Source\mapping_server\fish" "C:\Users\u0113054\OneDrive - KU Leuven\Source\fish"
````

Create a local network:
````commandline
docker network create mysqldatabase
````

## Local database
To load data in the local database, you should place the file in F:\unprotected_data\docker_input, create the database in an editor like the database panel in pycharm and then import the data using the commands:
(If you do not have the database files, please follow the guide in FISh to retrieve the documents and build the dataset.)
````commandline
# On linux
cat database.sql.gz | gunzip | sudo docker exec -i mariadb /usr/bin/mysql  -u root --password=<pw>

# On Windows, acces the shell of the docker container and then import the data
docker ps
docker exec -ti mariadb sh
cd /usr/input
cat documentsource2.sql.gz | gunzip |  /usr/bin/mysql  -u root --password=<pw> documentsource2
````