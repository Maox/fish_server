FROM registry.gitlab.com/maox/python_base

COPY monty /monty
RUN pip install -e /monty

COPY flock /flock
RUN pip install -e /flock
#
WORKDIR /app
COPY mapping_server/server .


#ENTRYPOINT ["/entrypoint.sh"]
CMD ["gunicorn","--timeout", "120" ,"-b", "0.0.0.0:8080","wsgi:app"]
