#!/bin/python

__author__ = 'Mart'
__date__ = '20/03/2019'
__doc__ = "Mapping host."

import base64
import io
import json
import logging
import os
import socket
import time

# logger = logging.getLogger(__name__)
# Expose the environment variables from os.getenv("xxx")
from dotenv import load_dotenv
from flask import Flask, request, render_template, send_file
from flask_cors import CORS
from monty.bridge.database.database import generate_validation_session
from monty.bridge.loupe.find_organism_documents.find_organism_documents import OrganismSearchHost
from monty.search.contracts.search_output import SearchQuery
from monty.search.manager import SearchManager
from monty.tricks.mlogger import MLogger

from flock.cluster_host import ClusterHost

# todo: remove this from startup and add it to the container.
import nltk
nltk.download('stopwords')

hostname = socket.gethostname()

mlogger = MLogger(name=hostname, session=generate_validation_session())

app = Flask(__name__)
CORS(app)

use_compressed = not os.environ.get('DEVELOPMENT_MACHINE', False)
search_host = SearchManager(use_compressed=use_compressed)
cluster_host = ClusterHost()

organism_search_host = OrganismSearchHost()

search_error = {
    "is_error": True,
    "error_text": "Something went wrong whilst querying the database.",
    "cached": False,
    }


@app.before_request
def log_request_info():
    pass
    # app.logger.debug('Headers: %s', request.headers)
    # app.logger.debug('Body: %s', request.get_data())


@app.route('/search', methods=["GET", 'POST'])
def return_search():
    """
    Search a text. This text is first preprocessed and then searched as a vector in the space.
    Returns JSON in the standard format
    """
    # logging.info("Handling search request...")
    if request.method == 'POST':
        data = request.get_json()
        query: SearchQuery = data.get("query", None)
        search_settings = data.get("search_settings", {})
        db = search_settings.get("db", None)
        idcookie = search_settings.get("idcookie", "nocookie")
        search_method = search_settings.get("search_method", "space")
        filter_lemmatized_query = search_settings.get("filter_lemmatized_query", True)
        if db is None:
            search_error["error_text"] = "Did not specify the database"
            return search_error
        if query is None:
            search_error["error_text"] = "Did not specify a search query!"
            return search_error
        if len(query["tokenized_query"]) + len(query["to_be_tokenized"]) < 1:
            search_error["error_text"] = "Did not specify a search query!"
            return search_error
        # logging.info("Searching in database: " + str(db))
        start_time = time.time()
        try:
            out = search_host.search_query(query, db, search_method,
                                           filter_lemmatized_query=filter_lemmatized_query, logger=app.logger)
        except Exception as e:  # todo: make this a contracted error message!
            print("Something went wrong")
            print(e)
            search_error["error_text"] = "Something went horribly wrong whilst searching."
            return search_error
        logging.info(
            "Found results in " + str(time.time() - start_time) + "s for query '"
            + ",".join(query["tokenized_query"])
            + query["to_be_tokenized"] + "'.")
        decoded_out = json.loads(out)
        if decoded_out.get("is_error", False) is True:
            mlogger.log_action("Error", json.dumps({
                "query": query,
                "error_text": decoded_out.get("error_text", "Failed to supply error text" + "-" + idcookie)
                }))
        else:
            mlogger.log_action("Search", json.dumps({
                "query": query,
                "query_info": decoded_out.get("query_info", None),
                "error_text": "-" + idcookie
                }))
        return out
    else:
        return "Error. This endpoint only supports POST requests."


@app.route('/cluster', methods=["GET", 'POST'])
def return_cluster():
    """
    Cluster the different texts!
    """
    logging.info("Handling clustering request...")
    if request.method == 'POST':
        try:
            data = request.get_json()
            db = data.get("db", None)
            cluster_settings = data.get("cluster_settings", None)
            if db is None:
                return '{"Error": "didn\'t specify database!"}'
            if cluster_settings is None:
                return '{"Error": "didn\'t specify cluster settings!"}'
            logging.info("Clustering in database: " + str(db))
            out = cluster_host.handle_clustering(db, cluster_settings)
            return out
        except Exception as e:
            logging.error("Error during clustering", exc_info=e)
            return {
                "clusters": [],
                "cluster_info": {"is_error": True, "error_text": "Sorry. Something went wrong on our end."}
                }
    else:
        return "Error. This endpoint only supports POST requests."


@app.route('/summarize', methods=["GET", 'POST'])
def return_summarization():
    logging.info("Handling summarization request...")
    if request.method == 'POST':
        try:
            data = request.get_json()
            summarization_settings = data.get("summarization_settings", None)
            cluster_json = data.get("cluster", None)
            if summarization_settings is None or cluster_json is None:
                return '{"Error": "didn\'t specify cluster settings!"}'
            out = cluster_host.handle_summarization(cluster_json, summarization_settings)
            return out
        except Exception as e:
            logging.error("Error during summarization", exc_info=e)
            return {
                "clusters": [],
                "cluster_info": {"is_error": True, "error_text": "Sorry. Something went wrong on our end."}
                }


@app.route('/find_organism', methods=["GET", 'POST'])
def return_find_relevant_organisms():
    logging.info("Handling find organism request...")
    if request.method == 'POST':
        try:
            data = request.get_json()
            query = data.get("query", None)
            if query is None:
                return '{"Error": "didn\'t specify query!"}'
            out = organism_search_host.find_possible_organisms_string(query)
            return json.dumps(out)
        except Exception as e:
            logging.error("Error during organism search", exc_info=e)
            return {
                "clusters": [],
                "cluster_info": {"is_error": True, "error_text": "Sorry. Something went wrong on our end."}
                }


@app.route('/get_organism_documents', methods=["GET", 'POST'])
def get_organism_documents():
    logging.info("Handling get organism documents request...")
    if request.method == 'POST':
        try:
            data = request.get_json()
            selected_id = data.get("organism_id", None)
            if selected_id is None:
                return '{"Error": "didn\'t specify query!"}'
            out = organism_search_host.get_orgid_documents(selected_id)
            return json.dumps(out)
        except Exception as e:
            logging.error("Error during organism search", exc_info=e)
            return {
                "clusters": [],
                "cluster_info": {"is_error": True, "error_text": "Sorry. Something went wrong on our end."}
                }


# @app.route('/search_pas', methods=["GET", 'POST'])
# def return_search_pas():
#     """
#     Search a combination of different product aspects.
#     This search method is also abstracted out to another file.
#     Returns JSON in the standard format
#     """
#     if request.method == 'POST':
#         data = request.get_json()
#         print(data)
#         pas = data.get("pas", [])
#         db = data.get("db", None)
#         if db is None:
#             return "Error: didn't specify database!"
#         if len(pas) < 1:
#             return "Error: didn't specify PAs!"
#         logging.info("Searching in database: " + str(db))
#         return search_pas(pas, db)
#     else:
#         return "Error. This endpoint only supports POST requests."


@app.route('/')
def return_index():
    return "Hello world"


@app.route("/gif")
def gif():
    gif = 'R0lGODlhAQABAIAAAP///////yH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='
    gif_str = base64.b64decode(gif)
    return send_file(io.BytesIO(gif_str), mimetype='image/gif')


if __name__ == "__main__":
    print('App is now running @ port 80')
    app.run(host='0.0.0.0', port=8080, debug=True, use_reloader=False)
